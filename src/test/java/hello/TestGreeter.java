package hello;

// on line change
// Pedro just adding another comment

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;


// Starting class (3-4)


public class TestGreeter {

   private Greeter g = new Greeter();

   @Test
   @DisplayName("Test for Empty Name")
   public void testGreeterEmpty() 

   {
      assertEquals(g.getName(),"");
      assertEquals(g.sayHello(),"Hello!");
   }

   @Test
   @DisplayName("Test for Ted")
   public void testGreeterTed() 

   {
      g.setName("Ted");
      assertEquals(g.getName(),"Ted");
      assertEquals(g.sayHello(),"Hello Ted!");
   }


   @Test
   @DisplayName("Test for Husain")
   public void testGreeterHusan() 

   {
      g.setName("Husain");
      assertEquals(g.getName(),"Husain");
      assertEquals(g.sayHello(),"Hello Husain!");
   }


   @Test
   @DisplayName("Test for Name='World'")
   public void testGreeter() 
   {

      g.setName("World");
      assertEquals(g.getName(),"World");
      assertEquals(g.sayHello(),"Hello World!");
   }


   @Test
   @DisplayName("Test for Jason")
   public void testGreeterJason() 

   { 
      g.setName("Jason");
      assertEquals(g.getName(),"Jason");
      assertEquals(g.sayHello(),"Hello Jason!");
   }

   @Test
   @DisplayName("Test for Capitilization")
   public void testGreeterCaps() 

   { 
      g.setName("JASON");
      assertFalse((g.getName() == "Jason"), "Cap Failure");
      assertFalse((g.sayHello() == "Hello Jason!"), "Cap Failure");
   }
   
   // Andres Correa new entries. 
   @Test
   @DisplayName("Test for Andres")
   public void testGreeterAndres()
   
   {
	   g.setName("Andres");
	   assertEquals(g.getName(),"Andres");
	   assertEquals(g.sayHello(),"Hello Andres!");
   }
   
   @Test
   @DisplayName("Test for special char")
   public void testGreeterSpecial()
   
   {
	   g.setName("!@#$%^&*()-=+_`~");
	  assertFalse((g.getName() == "123457890-=`"), "Symbol not here");
      assertFalse((g.sayHello() == "Hello 123457890-=`!"), "Symbol not here.");
   }

   @Test
   @DisplayName("Test for Pedro")
    public void testGreeterPedro() 

   { 
      g.setName("Pedro");
      assertEquals(g.getName(),"Pedro");
      assertEquals(g.sayHello(),"Hello Pedro!");
   }
   
   @Test
   @DisplayName("Test for Food")
    public void testGreeterFood() 

   { 
      g.setName("Food");
      assertFalse((g.getName() == "Water"), "Water is not food!");
      assertFalse((g.sayHello() == "Hello Water!"), "Water is not food!");
   }


   @Test
   @DisplayName("Test for Spacing")
   public void testGreeterSpacing()
   {
	   g.setName("Kevin ");
	   assertFalse((g.getName() == "Kevin") , "Extra Space in name");
      assertFalse((g.sayHello() == "Hello Kevin!"), "Extra Space in name");
   }

   @Test
   @DisplayName("Test 2 greetings")
   public void testSameGreeting()
   {
       Greeter h = new Greeter();
      g.setName("Test");
      h.setName("Test");
      assertEquals(g.getName(),"Test");
      assertEquals(h.getName(),"Test");
   }

   @Test
   @DisplayName("Test for Assignment")
   public void testGreeterAssignment()
   {
	  g.setName("Assignment Fourteen");
      assertEquals(g.getName(),"Assignment Fourteen");
      assertEquals(g.sayHello(),"Hello Assignment Fourteen!");
   }
   
}